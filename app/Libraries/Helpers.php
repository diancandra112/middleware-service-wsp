<?php

class Helpers
{
    
    public static function storeImageBase64encode($base64encode, $path_folder)
    {
        if (preg_match('/^data:image\/(\w+);base64,/', $base64encode)) {
            $image_base64encode = substr($base64encode, strpos($base64encode, ",")+1);
            $base64encode_name = time() . '.' . str_random(20) . '.jpg'; 
            $image_path = public_path() . $path_folder . $base64encode_name;
            file_put_contents($image_path, base64_decode($image_base64encode));
            $image_url = env('API_URL'). $path_folder . $base64encode_name;
        } else {
            $image_url = env('API_URL'). "/other/no_image.png";
        }

        return $image_url;
    }

    public static function ParseRowData($Row_Data)
    {
        $datas = str_split($Row_Data,2);
        $results=[];
        for ($i=0; $i < count($datas); $i++) { 
            $binary =self::hexBinSplit($datas[$i]);
            $type = bindec($binary[0]);
            $length =pow(2,bindec($binary[1]));
            switch ($type) {
                case 0:
                    // echo "Platform";
                    $input=self::getInput($datas,$i,$length);
                    // $as =self::hexBin($as[0]);
                    // print_r($input);
                    $i+=$length;
                    break;
                
                case 1:
                    // echo "MeterReading";
                    $input=self::getInput($datas,$i,$length);
                    $output=self::MeterReading($input);
                    $results=array_merge($results, $output);
                    // $as =self::hexBin($as[0]);
                    // print_r($output);
                    $i+=$length;
                    break;

                case 2:
                    //  echo "Alarm";
                    $input=self::getInput($datas,$i,$length);
                    $binary =self::hexBin($input[0]);
                    $output = self::alarm($binary);
                    $results=array_merge($results, $output);
                    // print_r($output);
                    $i+=$length;
                    break;

                case 3:
                    // echo "Status";
                    $input=self::getInput($datas,$i,$length);
                    $output=self::Status($input[0]);
                    $results=array_merge($results, $output);
                    // $as =self::hexBin($as[0]);
                    // print_r($output);
                    $i+=$length;
                    break;

                case 4:
                    // echo "Repayment";
                    $input=self::getInput($datas,$i,$length);
                    // $as =self::hexBin($as[0]);
                    // print_r($input);
                    $i+=$length;
                    break;

                case 5:
                    // echo "Uptime";
                    $input=self::getInput($datas,$i,$length);
                    // $as =self::hexBin($as[0]);
                    // print_r($input);
                    $i+=$length;
                    break;

                default:
                    // echo "==";
                    break;
            }
        }
        return $results;
    }

    public static function alarm($input)
    {   
        $output="";
        for ($i=0; $i < strlen($input) ; $i++) { 
            switch ($i) {
                case 0:
                    $output .= ($input[$i]==1) ? "Reboot, " : null ;
                    break;
                case 1:
                    $output .= ($input[$i]==1) ? "Battery low, " : null ;
                    break;
                case 2:
                    $output .= ($input[$i]==1) ? "No prepayment, " : null ;
                    break;
                case 3:
                    $output .= ($input[$i]==1) ? "Steal, " : null ;
                    break;
                case 4:
                    $output .= ($input[$i]==1) ? "Valve closed, " : null ;
                    break;
                case 5:
                    $output .= ($input[$i]==1) ? "No Signal, " : null ;
                    break;
                case 6:
                    $output .= ($input[$i]==1) ? "Valve fault, " : null ;
                    break;
                case 7:
                    $output .= ($input[$i]==1) ? "Reserve, " : null ;
                    break;
                default:
                    break;
            }
        }
        if (strlen($output)>0) {
            $output=substr($output,0,strlen($output)-2);
            $output=["alarm"=>$output];  
        }else{
            $output=["alarm"=>""];  
        }
        
        return $output;
    } 

    public static function MeterReading($input)
    {
        $hexString="";
        foreach ($input as $value){
            $hexString .=$value;
        }
        $binNumeric = hexdec($hexString);
        $length=strlen($binNumeric);
        $output = ["meterReading"=>substr($binNumeric,0,$length-3).".".substr($binNumeric,$length-3)];
        return $output;
    }

    public static function Status($input)
    {
        $outputValve=[];
        $outputBattry=[];
        $output=[];
        $binary =self::hexBin($input);
        if($binary[0]==1){
            $outputValve=["valve"=>"Open"];
        }
        else{
            $outputValve=["valve"=>"Close"];
        }
        $battry=substr($binary,1);
        $battry=self::binDec($battry);
        $outputBattry=["battery"=>$battry."%"];
        $output=array_merge($outputValve,$outputBattry);
        return $output;
    }

    public static function getInput($dataArray, $lastRead, $dataLength)
    {   $output=[];
        if ($dataLength==1) {
            array_push($output, $dataArray[$lastRead+1]);
            return $output;
        }
        else{
            $length=$lastRead+$dataLength+1;
            for ($i=$lastRead+1; $i < $length; $i++) { 
                array_push($output, $dataArray[$i]); 
            }
            return $output;
        }

    }

    public static function hexBinSplit($hexString)
    {
        $binNumeric = hexdec($hexString);
        $binString = decbin($binNumeric);
        $binString = str_pad($binString,8,0,STR_PAD_LEFT);
        $binaryArray = str_split($binString,strlen($binString)-2);
        return $binaryArray;
    }

    public static function hexBin($hexString)
    {
        $binNumeric = hexdec($hexString);
        $binString = decbin($binNumeric);
        $binString = str_pad($binString,8,0,STR_PAD_LEFT);
        return $binString;
    }

    public static function binDec($binString)
    {
       $binNumeric = bindec($binString); 
       return $binNumeric;
    }
}