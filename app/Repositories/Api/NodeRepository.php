<?php

namespace App\Repositories\Api;

use Carbon\Carbon;
use DateTime;
use Helpers;

class NodeRepository {

    public function findByDeviceEUI($deviceEUI)
    {
        try {
            $client = new \GuzzleHttp\Client();

            $headers = [
                // 'Authorization' => AuthRepository::getToken(),
                'Content-Type' => 'application/json'
            ];
            $res = $client->get(env('NS_API_HOST').'/device/uplink/'.$deviceEUI.'/data?token='.env('TOKEN_WSP').'&length=1',[
                'headers' => $headers,
            ]);
            $statusCode = $res->getStatusCode();
            $response = json_decode($res->getBody());
            $data = $response->data[0];
            $datetime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $data->time);
            $last_update = $datetime->format('Y-m-d H:i:s');
            

            $array = [];
            $rowData = Helpers::ParseRowData($data->raw_data);
            // $rowData = Helpers::ParseRowData("08a40ce40611111111");
            array_push($array,[
                'time' => $data->time,
                'bw' => $data->bw,
                'datasize' => $data->datasize,
                'devaddr' => $data->devaddr,
                'deveui' => $data->deveui,
                'devname' => $data->devname,
                'fport' => $data->fport,
                'gateway_eui' => $data->gateway_eui,
                'gateway_list' => $data->gateway_list,
                'rssi' => $data->rssi,
                'uplink_count' => $data->uplink_count,
                'sf' => $data->sf,
                'freq' => $data->freq,
                'raw_data' => $data->raw_data,
                'snr' => $data->snr,
                'owner' => $data->owner,
                'last_update' => $last_update
            ]);
            $output=array_merge($array[0],$rowData);
            $result = [
                'status' => $statusCode,
                'data' => $output,
                // 'result' =>$array
            ];
            return $result;

        } catch (\Exception $e) {
            $res = $e->getResponse();
            $statusCode = $res->getStatusCode();
            $response = json_decode($res->getBody());

            $result = [
                'status' => $statusCode,
                'data' => $response
            ];
            return $result;

        }
    }
}